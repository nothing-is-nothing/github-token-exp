package github

import (
	"encoding/json"
	"net/http"

	"gitee.com/nothing-is-nothing/httpclient"
	"gitee.com/nothing-is-nothing/logger"
)

type Repo struct {
	ID          int    `json:"id"`
	NodeID      string `json:"node_id"`
	Name        string `json:"name"`
	FullName    string `json:"full_name"`
	Private     bool   `json:"private"`
	Description string `json:"description"`
	Owner       struct {
		Login      string `json:"login"`
		ID         int    `json:"id"`
		NodeID     string `json:"node_id"`
		AvatarURL  string `json:"avatar_url"`
		GravatarID string `json:"gravatar_id"`
		URL        string `json:"url"`
	}
}

func GetPrivateRepo(token string) ([]Repo, error) {
	req, err := http.NewRequest("GET", "https://api.github.com/user/repos?type=private", nil)
	if err != nil {
		logger.Error("http.NewRequest failed,err:%v\n", err)
		return nil, err
	}
	req.Header.Add("Authorization", "token "+token)
	resp, err := httpclient.Do(req)
	if err != nil {
		logger.Error("httpclient.Do failed,err:%v\n", err)
		return nil, err
	}
	var data []Repo
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		logger.Error("json.NewDecoder failed,err:%v\n", err)
		return nil, err
	}
	return data, nil
}

func GetPublicRepo(token string) ([]Repo, error) {
	req, err := http.NewRequest("GET", "https://api.github.com/user/repos?type=public", nil)
	if err != nil {
		logger.Error("http.NewRequest failed,err:%v\n", err)
		return nil, err
	}
	req.Header.Add("Authorization", "token "+token)
	resp, err := httpclient.Do(req)
	if err != nil {
		logger.Error("httpclient.Do failed,err:%v\n", err)
		return nil, err
	}
	var data []Repo
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		logger.Error("json.NewDecoder failed,err:%v\n", err)
		return nil, err
	}
	return data, nil
}
