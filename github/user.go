package github

import (
	"encoding/json"
	"net/http"

	"gitee.com/nothing-is-nothing/httpclient"
)

type User struct {
	Login string `json:"login"`
	Email string `json:"email"`
	Name  string `json:"name"`
}

// 获取当前用户信息
func GetCurrentUser(token string) (*User, error) {
	req, err := http.NewRequest("GET", "https://api.github.com/user", nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", "token "+token)

	resp, err := httpclient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var userData User
	err = json.NewDecoder(resp.Body).Decode(&userData)
	if err != nil {
		return nil, err
	}

	return &userData, nil
}
