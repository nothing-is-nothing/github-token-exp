module gitee.com/nothing-is-nothing/github-token-exp

go 1.22.1

require (
	gitee.com/nothing-is-nothing/httpclient v1.0.23
	gitee.com/nothing-is-nothing/logger v1.0.1
)

require (
	gitee.com/nothing-is-nothing/mycolor v1.0.0 // indirect
	gitee.com/nothing-is-nothing/rawhttp v1.0.3 // indirect
	gitee.com/nothing-is-nothing/utils v1.0.20 // indirect
	github.com/gookit/color v1.5.4 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.5 // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/olekukonko/tablewriter v0.0.5 // indirect
	github.com/xo/terminfo v0.0.0-20210125001918-ca9a967f8778 // indirect
	golang.org/x/net v0.22.0 // indirect
	golang.org/x/sys v0.18.0 // indirect
)
