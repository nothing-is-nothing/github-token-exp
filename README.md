用于证明泄漏的github api token 可用的工具

安装:

```
go install gitee.com/nothing-is-nothing/github-token-exp@latest
```

使用:

```
github-token-exp -token <github api token>
```