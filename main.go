package main

import (
	"flag"
	"fmt"

	"gitee.com/nothing-is-nothing/github-token-exp/github"
	"gitee.com/nothing-is-nothing/httpclient"
	"gitee.com/nothing-is-nothing/logger"
)

func main() {

	var token string
	var proxy string
	var debug bool

	flag.StringVar(&token, "token", "", "github token")
	flag.StringVar(&proxy, "proxy", "", "proxy")
	flag.BoolVar(&debug, "debug", false, "debug")

	flag.Parse()

	if token == "" {
		logger.Error("token is empty\n")
		flag.PrintDefaults()
		return
	}
	logger.Init(debug)

	if err := httpclient.Init(proxy, debug); err != nil {
		logger.Error("httpclient.Init failed,err:%v\n", err)
		return
	}

	// 获取当前用户信息
	user, err := github.GetCurrentUser(token)
	if err != nil {
		logger.Error("getCurrentUser failed,err:%v\n", err)
		return
	}
	fmt.Printf("当前用户:\n- username: %s\n- email: %s\n", user.Login, user.Email)

	// 获取私有仓库
	privateRepo, err := github.GetPrivateRepo(token)
	if err != nil {
		logger.Error("getPrivateRepo failed,err:%v\n", err)
		return
	}
	fmt.Println("私有仓库:")
	if len(privateRepo) == 0 {
		fmt.Println("- 无私有仓库")
	} else {
		for _, repo := range privateRepo {
			fmt.Printf("- https://github.com/%s [private:%v] %s\n", repo.FullName, repo.Private, repo.Description)
		}
	}

	// 获取公共仓库
	publicRepo, err := github.GetPublicRepo(token)
	if err != nil {
		logger.Error("getPublicRepo failed,err:%v\n", err)
		return
	}
	fmt.Println("公共仓库:")
	for _, repo := range publicRepo {
		fmt.Printf("- https://github.com/%s [private:%v] %s\n", repo.FullName, repo.Private, repo.Description)
	}
}
